import React, { useContext } from "react";
import { StoreContext } from './stores';
import { BrowserRouter, Navigate } from 'react-router-dom';
import Navbar from './components/Navbar/Navbar';
import Authorization from "./components/Authorization/Authorization";
import Rating from "./components/Rating/Rating";
import GameField from "./components/GameField/GameField";
import AppRouter from "./components/AppRouter/AppRouter";
import NotFound from './components/404/404';
import './assets/style/App.css'

const App = () => {

  const routes = [
    {
      path: '/game',
      title: 'Игровое поле',
      component: <GameField/>
    },
    {
      path: '/rating',
      title: 'Рейтинг',
      component: <Rating/>
    },
    {
      path: '/active',
      title: 'Активные игроки',
      component: <NotFound/>
    },
    {
      path: '/history',
      title: 'История игр',
      component: <NotFound/>
    },
    {
      path: '/players',
      title: 'Список игроков',
      component: <NotFound/>
    },
    {
      path: '/',
      component: <Navigate replace to="/rating"/>,
    },
    {
      path: '*',
      component: <NotFound/>
    }
  ];

  return (
    <div className="app">

      <BrowserRouter>
        
        <AppRouter routes={routes} />
      
      </BrowserRouter>

    </div>
  );
}

export default App;
