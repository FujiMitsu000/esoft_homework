import React from 'react'
import './404.css';

const NotFound = () => {
  return (
    <div className='not_found'>
      404
    </div>
  )
}

export default NotFound