import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite';
import { StoreContext } from '../../stores';
import { Route, Routes } from 'react-router-dom';
import Navbar from '../Navbar/Navbar';
import Authorization from '../Authorization/Authorization';


const AppRouter = observer((props) => {
    const Store = useContext(StoreContext);

  return (
    <Routes>
        {props.routes.map(route =>
            <Route
                key={route.title}
                path={route.path}
                element={
                    (Store.AuthStore.Authorized
                    ?
                    <>
                        <Navbar routes={props.routes}/>  
                        {route.component}
                    </>
                    :
                    <Authorization/>
                    )
                }
            />
        )}
    </Routes>
  )
})

export default AppRouter