import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { StoreContext } from '../../stores';
import Dog from '../../assets/image/dog.png';
import './authorization.css'



const Authorization = observer(() => {
    const Store = useContext(StoreContext);

  return (
    <main className="box login">
        <img className="login-dog" src={Dog} alt="Собака"/>
        <h1 className="text text_title">Войдите в игру</h1>
        <form className="login_form">
            <div className="input form_field">
                <input
                    className="text input_textbox"
                    type="text"
                    placeholder="Логин"
                    onCopy={(e) => Store.AuthStore.preventCopy(e.target)}
                    onCut={(e) => Store.AuthStore.preventCopy(e.target)}
                    onChange={e => {
                        Store.AuthStore.preventCyrillic(e.target)
                        Store.AuthStore.setLogin(e.target.value)
                    }}
                />
            </div>
            <div className="input form_field">
                <input
                    className="text input_textbox"
                    type="password"
                    placeholder="Пароль"
                    onCopy={(e) => Store.AuthStore.preventCopy(e.target)}
                    onCut={(e) => Store.AuthStore.preventCopy(e.target)}
                    onChange={e => {
                        Store.AuthStore.preventCyrillic(e.target)
                        Store.AuthStore.setPassword(e.target.value)
                    }}
                />
            </div>
            <input
                className="button button_primary form_submit login_submit"
                type="submit"
                value="Войти"
                onClick={(e) => {
                    e.preventDefault(); 
                    Store.AuthStore.login();
                }}
            />
        </form>
    </main>
  )
})

export default Authorization