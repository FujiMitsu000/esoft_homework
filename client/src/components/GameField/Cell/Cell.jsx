import React from 'react'
import './cell.css'
import {ReactComponent as Cross} from '../../../assets/svg/Cross.svg';
import {ReactComponent as Circle} from '../../../assets/svg/Circle.svg'



const Cell = (props) => {
  return (
    <div className='gameField_cell'>
      {props.symbol ?
        props.symbol === 'X' ? <Cross className='cross'/> : <Circle className='circle'/> 
        : 
        ''
      }
    </div>
  )
}

export default Cell;
