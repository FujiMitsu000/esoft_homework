import React, { useRef, useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { StoreContext } from '../../stores';
import {ReactComponent as CrossMini} from '../../assets/svg/CrossMini.svg';
import {ReactComponent as CircleMini} from '../../assets/svg/CircleMini.svg';
import {ReactComponent as SendIcon} from '../../assets/svg/Send.svg';
import Cell from './Cell/Cell';
import './gameField.css';

const GameField = observer(() => {
    const Store = useContext(StoreContext);
    const gameFieldRef = useRef();

    function renderCell(symbol, id) {
        return (
            <Cell 
                key={id}
                symbol={symbol}
            />
        );
    }

  return (
    <div className="game">
        <aside className="players">
            <h1 className="text text_title">Игроки</h1>
            <div className="player-stat">
                {
                <>
                    <CircleMini className="player-icon-size icon-circle-color"/>
                    <div>
                        <p className="text player-stat-name">{Store.GameStore.players[1].fullName}</p>
                        <small className="player-stat-win-rate">63% побед</small>
                    </div>
                </>
                }
            </div>
            <div className="player-stat">
                {
                <>
                    <CrossMini className="player-icon-size icon-cross-color"/>
                    <div>
                        <p className="text player-stat-name">{Store.GameStore.players[0].fullName}</p>
                        <small className="player-stat-win-rate">23% побед</small>
                    </div>
                </>
                }
            </div>
        </aside>
        <div className="game-info">
            <div className="game-timer">05:12</div>
            <main 
                className="game-field"
                ref={gameFieldRef} 
                onClick={(e) => Store.GameStore.makeMove(e, gameFieldRef)}
            >
                {
                    Store.GameStore.field.map((symbol, index) => renderCell(symbol, index))
                }
            </main>
            <div className='current-turn'>
                <p className="text">Ходит</p>
                {
                    Store.GameStore.currentTurn % 2 ?
                    <>
                        <CircleMini className="player-icon-size icon-circle-color"/>
                        <p className="text">{Store.GameStore.players[1].username}</p>
                    </>
                    :
                    <>
                        <CrossMini className="player-icon-size icon-cross-color"/>
                        <p className="text">{Store.GameStore.players[0].username}</p>
                    </>
                }
            </div>
        </div>
            <aside className="chat">
                <div className="chat-messages">
                    <div className='chat-message chat-message_left'>
                        <div className="chat-message-header">
                            <p className='chat-sender-name sender-name-left-color'>
                                имя фамилия
                            </p>
                            <p className="send-time">
                                13:13
                                </p>
                        </div>
                        <p className="message-text">
                            текст
                            </p>
                    </div>
                    <div className='chat-message chat-message_right'>
                        <div className="chat-message-header">
                            <p className='chat-sender-name sender-name-right-color'>
                                имя фамилия
                            </p>
                            <p className="send-time">
                                13:13
                                </p>
                        </div>
                        <p className="message-text">
                            текст
                            </p>
                    </div>
                </div>
                <div className="send">
                    <div className="input send-input">
                        <input
                            className="input__textbox"
                            placeholder="Сообщение..."
                        />
                    </div>
                    <button className="button button_primary send-btn">
                        <SendIcon className="send-icon"/>
                    </button>
                </div>
            </aside>
        </div>
  )
})

export default GameField