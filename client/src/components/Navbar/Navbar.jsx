import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { StoreContext } from '../../stores';
import {ReactComponent as Logo} from '../../assets/svg/logo.svg';
import {ReactComponent as ExitIcon} from '../../assets/svg/ExitIcon.svg';
import { Link } from 'react-router-dom';
import './navbar.css';

const Navbar = observer((props) => {
    const Store = useContext(StoreContext);

  return (
    <nav onClick={e => e.stopPropagation()}>
        <Logo 
            className='svg_logo_icon'
        />
        <div
            className='navbar'
            onClick={(e) => Store.NavbarStore.activeLink(e)}
        >
            {
                props.routes.map((route, index) => 
                    <Link
                        key={index}
                        id={index}
                        className='link text'
                        to={route.path}
                    >
                        {route.title}
                    </Link>
                )
            }
        </div>
        <ExitIcon
            className='svg_exit_icon'
        />
    </nav>
  )
})

export default Navbar