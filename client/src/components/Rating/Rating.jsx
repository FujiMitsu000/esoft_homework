import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { StoreContext } from '../../stores';
import './rating.css'

const Rating = () => {
    const Store = useContext(StoreContext);

  return (
    <main className="box rating">
        <h1 className="text text_title box__header rating__title">Рейтинг игроков</h1>
        <table className="rating__table">
            <thead>
                <tr className="table__row">
                    <th className="text text_bold table__cell">ФИО</th>
                    <th className="text text_bold table__cell">Всего игр</th>
                    <th className="text text_bold table__cell">Победы</th>
                    <th className="text text_bold table__cell">Проигрыши</th>
                    <th className="text text_bold table__cell">Процент побед</th>
                </tr>
            </thead>
            <tbody>
                {
                    Store.RatingStore.players.map((player) => (
                        <tr key={player.fullName} className="table__cell">
                            <th className="text table__cell rating__name">{player.fullName}</th>
                            <th className="text table__cell">{player.totalGame}</th>
                            <th className="text table__cell rating__wins">{player.wins}</th>
                            <th className="text table__cell rating__loses">{player.loses}</th>
                            <th className="text table__cell">{player.winRate}%</th>
                        </tr>
                    ))
                }
            </tbody>
        </table>
    </main>
  )
}

export default Rating