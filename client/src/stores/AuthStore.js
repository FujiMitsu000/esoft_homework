import {makeAutoObservable} from 'mobx';

export default class NavbarStore {
    
    thisUser = {username: '', password: ''};
    Authorized = false;

    constructor(server) {
        makeAutoObservable(this);

        this.server = server;
    }

    setLogin(text) {
        this.thisUser.username = text;
    }

    setPassword(text) {
        this.thisUser.password = text;
    }

    preventCyrillic(target) {
        if (/^[a-zA-Z0-9._]*?$/.test(target.value)) 
            target.defaultValue = target.value;
        else 
            target.value = target.defaultValue;
    }

    preventCopy(target) {
        target.value = '';
        target.value = target.defaultValue;
    }

    login() {
        fetch (`http://localhost:8000/api/auth/login`, {
            method: 'POST',
            body: JSON.stringify(
                {
                    username: this.thisUser.username,
                    password: this.thisUser.password
                }
            ),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then((user) => {
            if (user) {
                console.log(user.msg);
                this.Authorized = true;
            }
        });
    }
}