import {makeAutoObservable} from 'mobx';

export default class NavbarStore {
    
    currentTurn = 0;
    currentFieldPosition = '';
    field = Array(9).fill(null);
    players = [
        {
            username: 'Екатерина Плюшкина',
            symbol: 'X',
            fullName: 'Плюшкина Екатерина Викторовна'
        },
        {
            username: 'Владлен Пупкин',
            symbol: 'O',
            fullName: 'Пупкин Владлен Игоревич'
        },
    ];

    constructor() {
        makeAutoObservable(this);
    }

    // основной обработчик(?). Надо вынести его из этого класса в отдельный фаил
    makeMove = (e, gameFieldRef) => {

        this.checkCells(e.target, gameFieldRef.current.children);

        if (!this.isPossibleMove(this.currentFieldPosition)){
            console.log('Клетка уже занята');
        } else {

            this.setSymbol(this.currentFieldPosition, this.getSymbol(this.currentTurn % 2));

            this.currentTurn++;
        }
    }

    // получение номера клетки на которую нажал игрок 
    checkCells = (cell, cells) => {
        for (let idx = 0; idx < cells.length; idx++) {
            if (cell === cells[idx]){
                this.currentFieldPosition = idx;
                break;
            }
        }
    }

    // проверка возможен ли ход 
    isPossibleMove(position) {
        return position >= 0 && position < this.field.length && this.field[position] === null;
    }

    // получает символ
    getSymbol(idx) { 
        return this.players[idx].symbol;
    }

    // запись символа в массив поля
    setSymbol(position, symbol) {
        this.field[position] = symbol;
    }
    
}