import {makeAutoObservable} from 'mobx';

export default class NavbarStore {
    
    menuLinks = ['Игровое поле', 'Рейтинг', 'Активные игроки', 'История игр', 'Список игроков'];
    linkActive = false;

    constructor() {
        makeAutoObservable(this);
    }

    activeLink = (e) => {
        if (e.target.id !== '') {
            let arr = Array.from(e.currentTarget.children);

            arr.map(link => link.classList.remove("link_active"))

            e.target.classList.add("link_active");
        }
    }
}