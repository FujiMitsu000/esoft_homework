import {makeAutoObservable} from 'mobx';

export default class NavbarStore {
    
    players = [
        {fullName: 'Александров Игнат Анатолиевич', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Шевченко Рафаил Михайлович', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Мазайло Трофим Артёмович', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Логинов Остин Данилович', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Александров Игнат Анатолиевич', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Александров Игнат Анатолиевич', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Александров Игнат Анатолиевич', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Александров Игнат Анатолиевич', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Александров Игнат Анатолиевич', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Александров Игнат Анатолиевич', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Александров Игнат Анатолиевич', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Александров Игнат Анатолиевич', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Александров Игнат Анатолиевич', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}, 
        {fullName: 'Александров Игнат Анатолиевич', totalGame: 24534, wins: 9826, loses: 1222, winRate: '87'}
    ];

    constructor() {
        makeAutoObservable(this);
    }
}