import { createContext } from "react";
import NavbarStore from "./NavbarStore";
import RatingStore from "./RatingStore";
import GameStore from "./GameStore";
import AuthStore from "./AuthStore";

export const StoreContext = createContext(null);
export class Store {

    constructor() {
        this.server = 'http://localhost:8000';

        this.NavbarStore = new NavbarStore();
        this.RatingStore = new RatingStore();
        this.GameStore = new GameStore();
        this.AuthStore = new AuthStore();
    }
}