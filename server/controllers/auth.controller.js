import isValidPassword from '../utils/comparePasswords.js';
import getHashPassword from '../utils/crypto.js';
import { 
    writeInFile as setPassword, 
    readFile as getPassword, 
    createFile
} from '../utils/fs.js';


async function createUser(req, res) {
    const {username, password} = req.body;

    createFile(`../server/db/users/${username}.txt`);

    const hashPassword = getHashPassword(password);

    setPassword(`../server/db/users/${username}.txt`, hashPassword);

    return res
        .status(200)
        .json({msg: `Пользователь ${username} создан`});
}

async function loginUser(req, res) {
    const {username, password} = req.body;

    const userExist = getPassword(`../server/db/users/${username}.txt`);

    if (!isValidPassword(userExist, password)) {
        return res
            .status(400)
            .json({msg: `Неверный пароль`});
    }

    return res
        .status(200)
        .json({msg: `Вход выполнен`});
}


export {
    createUser,
    loginUser,
}