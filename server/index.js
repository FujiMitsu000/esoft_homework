import express, { json, urlencoded } from 'express';
import routes from './routes/index.routes.js';
import { createServer } from 'http';
import config from './configs/index.js';


const PORT = config[process.env.NODE_ENV || 'development'].server.port;

const app = express();
const httpServer = createServer(app);

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization, authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});

app.use(json());
app.use(urlencoded({ extended: false}));

app.use('/api', routes);


async function startApp() {
    try {
        httpServer.listen(PORT, () => console.log(`SERVER STARTED ON PORT ${PORT}`))
    } catch (err) {
        console.log(err)
    }
};

startApp();