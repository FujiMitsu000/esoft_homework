import { compareSync } from 'bcrypt';

function isValidPassword (userPassword, password) {
    return compareSync(password, userPassword);
}

export default isValidPassword
