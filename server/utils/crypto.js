import { hashSync } from 'bcrypt';

function getHashPassword(password) {
    const hashPassword = hashSync(password, 8);
    
    return hashPassword;
}

export default getHashPassword
