import { writeFile, open, readFileSync } from 'node:fs';

function createFile(path){

    open(path, 'w', (err) => {
        if (err) throw err;
        console.log('Файл создан');
    });

}

function writeInFile(path, content) {

    writeFile(path, content, (err) => {
        if(err) throw err;
        console.log('Пароль записан');
    });

}

function readFile(path) {

    try {
        const data = readFileSync(path, 'utf8')
        return data
    } catch (err) {
        console.error(err)
    }

}

export {
    createFile,
    writeInFile,
    readFile
};